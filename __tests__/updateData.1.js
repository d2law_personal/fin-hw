const axios = require('axios');
var jequal = require('node-json-equal');
var bodyFormData = new FormData();
bodyFormData.append('key', 'secret key between client/server');

describe("Test getData", () => {

  var token;
  var axiosConfig = {
    headers: {
      'content-type': 'multipart/form-data'  // Is set automatically
    }
  }

  beforeAll(async (done) => {
    var data = await axios.post('http://localhost:8000/api/auth', bodyFormData, axiosConfig)
      .then(function (response) {
        return response.data;
      });

    token = {
      headers: {
        'authorization': `Bearer ${data.token} `
      }
    }

    done();
  })

  test("updateData Test case #1, data existed, update one row and one column", async () => {
    var updateBody = new FormData();

    updateBody.append('name', 'apple2');
    updateBody.append('valid', 0);

    var res = await axios.post('http://localhost:8000/api/updateData', updateBody, token)


    expect(res.data.message).toEqual("1 row(s) updated.");

    //I should check the insert record in DB too, but no time to implement 
  });

  test("updateData Test case #3, update 2 rows", async () => {
    var updateBody = new FormData();

    updateBody.append('name', 'pear');
    updateBody.append('valid', 0);
    updateBody.append('count', 999);

    var res = await axios.post('http://localhost:8000/api/updateData', updateBody, token)


    expect(res.data.message).toEqual("2 row(s) updated.");
  });


  test("updateData Test case #5, error return", async () => {
    var updateBody = new FormData();
    updateBody.append('name', 'apple2');
    updateBody.append('valid', 'A');


    var res = await axios.post('http://localhost:8000/api/updateData', updateBody, token)

    expect(res.data.error).toEqual("invalid type (BOOLEAN)");

  });

  test("updateData Test case #17, error return", async () => {
    var updateBody = new FormData();
    var res = await axios.post('http://localhost:8000/api/updateData', updateBody, token)

    expect(res.data.error).toEqual("empty input");

  });

});