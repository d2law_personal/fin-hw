const axios = require('axios');
var jequal = require('node-json-equal');
var bodyFormData = new FormData();
bodyFormData.append('key', 'secret key between client/server');

describe("Test getData", () => {

    var token;
    var axiosConfig = {
        headers: {
            'content-type': 'multipart/form-data'  // Is set automatically
        }
    }

    beforeAll(async (done) => {
        var data = await axios.post('http://localhost:8000/api/auth', bodyFormData, axiosConfig)
            .then(function (response) {
                return response.data;
            });

        token = {
            headers: {
                'authorization': `Bearer ${data.token} `
            }
        }

        done();
    })

    test("getData Test case #1, resturn single row", async () => {
        var res = await axios.get('http://localhost:8000/api/getData?name=apple', token)


        expect(res.data[0]).toEqual(
            {
                "name": "apple",
                "valid": 1,
                "count": 12
            }
        );
    });

    test("getData Test case #3, multiple rows return", async () => {
        var res = await axios.get('http://localhost:8000/api/getData?name=pear', token)
        expect(res.data.length).toEqual(2);
        var expect_result = {
            "name": "pear",
            "valid": 0,
            "count": 999
        };
        expect(res.data[0]).toEqual(expect_result);
        expect(res.data[1]).toEqual(expect_result);
    });


    test("getData Test case #5, multiple rows return", async () => {
        var res = await axios.get('http://localhost:8000/api/getData?count=3', token);
        expect(res.data.length).toEqual(0);

    });


});







