var express = require('express');
var router = express.Router();

const sqlite3 = require('sqlite3').verbose();

router.get('/getData', function (req, res) {
    const query = req.query;
    var spec_key = '';
    var data_key = '';

    if (Object.keys(query).length == 0) {
        res.json({ error: "empty input" });
        return;
    } else if (Object.keys(query).length > 1) {
        res.json({ error: "not support multiple fields" });
        return;
    } else {
        spec_key = Object.keys(query)[0];
        data_key = Object.values(query)[0];

        if (data_key instanceof Array) {
            res.json({ error: "not support multiple fields" });
            return;
        }
    }


    var db = new sqlite3.Database('./database/test.db', sqlite3.OPEN_READONLY);

    var spec1 = `select 'column' || rowid as col, column_name, (column_name = ?) as match from spec1 order by col`;
    console.log(spec1);
    var search_column = '';
    var isError = true;

    db.all(spec1, spec_key, (err, row) => {

        if (err) {
            res.json({ error: "database error" });
        }

        var dataCol = '';
        for (var i = 0; i < row.length; i++) {

            if (row[i].match) {
                search_column = row[i].col + '= ? ';

                //check null value
                if (data_key == '') {
                    search_column += ' or ' + row[i].col + ' is null';
                }
            }

            dataCol += row[i].col + ' as ' + row[i].column_name;
            if (i < row.length - 1) {
                dataCol += ',';
            }

        }


        if (search_column == '') {
            res.json({ error: "column name not existed" });
        } else {
            var data1 = 'select ' + dataCol + ' from data1 where ' + search_column;

            db.all(data1, data_key, (err, data_row) => {
                if (err) {
                    res.json({ error: "database error" });
                }
                res.json(data_row);
            })
        }
    });

    db.close();

});

module.exports = router;