var express = require('express');
var router = express.Router();

const sqlite3 = require('sqlite3').verbose();

router.post('/updateData', function (req, res) {
    const body = req.body;
    var name_key = '';
    var spec_key = [];
    var data_key = [];
    var spec_where = '';

    if (Object.keys(body).length == 0) {
        res.json({ error: "empty input" });
        return;
    } else if (!body.name) {
        res.json({ error: "name is missing" });
        return;
    } else if (Object.keys(body).length == 1) {
        res.json({ error: "require more input" });
        return;
    } else {
        name_key = body.name;
        if (name_key instanceof Array) {
            res.json({ error: "not support multiple fields" });
            return;
        }

        spec_key = Object.keys(body);
        for (var i = 0; i < spec_key.length; i++) {
            spec_where += 'column_name = ? ';
            if (i < spec_key.length - 1) {
                spec_where += ' or '
            }
        }
    }

    var db = new sqlite3.Database('./database/test.db', sqlite3.OPEN_READWRITE);


    var spec1 = `select 'column' || rowid as col, column_name, datatype from spec1 where ` + spec_where;

    var search_column = '';
    var set_stmt = '';
    var isError = false;

    db.all(spec1, spec_key, (err, row) => {

        if (err) {
            res.json({ error: "database error" });
        } else if (row.length != spec_key.length) {
            res.json({ error: "invalid column name" });
        } else {
            //datatype check, TEXT can be anything, BOOLEAN can only be 0 or 1, INTEGER can be number
            for (var i = 0; i < row.length; i++) {

                //check integer type or null

                if (row[i].datatype == 'INTEGER' && body[row[i].column_name] != '' && isNaN(body[row[i].column_name])) {
                    res.json({ error: "invalid type (INTEGER)" });
                    isError = true;
                } else if (row[i].datatype == 'BOOLEAN' && body[row[i].column_name] != '' && (body[row[i].column_name] != '0' && body[row[i].column_name] != '1')) {
                    res.json({ error: "invalid type (BOOLEAN)" });
                    isError = true;
                } else if (row[i].column_name == 'name') {
                    //get name column #
                    search_column = 'where ' + row[i].col + '= ? ';
                } else {

                    if (set_stmt != '') {
                        set_stmt += ',';
                    }

                    if (body[row[i].column_name] == '') {
                        // set null
                        set_stmt += row[i].col + '= null ';
                    } else {
                        set_stmt += row[i].col + '= ? ';
                        data_key.push(body[row[i].column_name]);
                    }

                }

                if (isError) {
                    break;
                }
            }

            if (!isError) {
                data_key.push(name_key);
                var updateStmt = 'update data1 set ' + set_stmt + search_column;

                db.run(updateStmt, data_key, function(err)  {

                    if (err) {
                        res.json({ error: "database error" });
                    } else {
                        res.json({message:  this.changes + " row(s) updated."});
                    }
                    
                });
            }
        }
    });

    db.close();

});

module.exports = router;